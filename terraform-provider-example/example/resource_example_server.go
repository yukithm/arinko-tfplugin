package example

import (
	"github.com/hashicorp/terraform/helper/schema"
	"gitlab.com/yukithm/arinko-tfplugin/terraform-provider-example/fakeservice"
)

func resourceExampleServer() *schema.Resource {
	return &schema.Resource{
		// CRUD
		// Existsというのもあるが、Readがきちんと実装されていればなくてもよい
		Create: resourceExampleServerCreate,
		Read:   resourceExampleServerRead,
		Update: resourceExampleServerUpdate,
		Delete: resourceExampleServerDelete,

		// 項目の定義
		Schema: map[string]*schema.Schema{
			"address": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
		},
	}
}

// Create: リソースを作成する
//
// SetIdしない場合、エラーの有無に関わらずリソースは作成されていないことになる。
// stateにも反映されない。
// SetIdした場合、エラーの有無に関わらずリソースは作成されたことになる。stateにも反映される。
func resourceExampleServerCreate(d *schema.ResourceData, m interface{}) error {
	client := m.(*fakeservice.Client)

	address := d.Get("address").(string)
	server, err := client.CreateServer(address)
	if err != nil {
		return err
	}

	// SetId()を呼ぶことでTerraformにリソースが作成されたことを伝える
	// この値は任意の文字列が指定できるが、リソースを再び読み出すときに使える値でなければならない
	// 要するにリソースのユニークなID
	d.SetId(server.ID)

	return resourceExampleServerRead(d, m)
}

// Read: 実リソースの情報を取得する
//
// リソースがなかった場合とそれ以外のエラーとを区別できるようにする必要がある。
// リソースがなかった場合はエラーではなく、d.SetId("")する。
func resourceExampleServerRead(d *schema.ResourceData, m interface{}) error {
	client := m.(*fakeservice.Client)

	server, err := client.GetServer(d.Id())
	if err != nil {
		if err == fakeservice.ErrServerNotExist {
			d.SetId("")
			return nil
		}
		return err
	}

	d.Set("address", server.Address)

	return nil
}

// Update: リソースを更新する
//
// エラーの有無に関わらずリソースは更新されたことになり、stateも更新される。
// もしIDをブランクに設定すると、リソースは削除されたことになる。
func resourceExampleServerUpdate(d *schema.ResourceData, m interface{}) error {
	client := m.(*fakeservice.Client)

	if d.HasChange("address") {
		address := d.Get("address").(string)
		_, err := client.UpdateServer(d.Id(), address)
		if err != nil {
			if err == fakeservice.ErrServerNotExist {
				d.SetId("")
			}
			return err
		}
	}

	return resourceExampleServerRead(d, m)
}

// Delete: リソースを削除する
//
// リソースがすでに削除されている場合にも考慮すること（手動で削除した場合など）。
// リソースがすでに削除されていた場合はエラーにはしないこと。
//
// エラーを返さない場合、リソースは削除されたことになり、stateから削除される。
// エラーを返す場合、リソースは残っているものとなり、stateも保持される。
func resourceExampleServerDelete(d *schema.ResourceData, m interface{}) error {
	client := m.(*fakeservice.Client)

	if err := client.DeleteServer(d.Id()); err != nil && err != fakeservice.ErrServerNotExist {
		return err
	}

	d.SetId("")
	return nil
}
