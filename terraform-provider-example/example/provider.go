package example

import (
	"github.com/hashicorp/terraform/helper/schema"
	"github.com/hashicorp/terraform/terraform"
	"gitlab.com/yukithm/arinko-tfplugin/terraform-provider-example/fakeservice"
)

func Provider() terraform.ResourceProvider {
	return &schema.Provider{
		Schema: map[string]*schema.Schema{
			"base_dir": {
				Type:        schema.TypeString,
				Optional:    true,
				Default:     "example-provider-data",
				Description: "The path to the provider data directory",
			},
		},
		ResourcesMap: map[string]*schema.Resource{
			"example_server": resourceExampleServer(),
		},
		ConfigureFunc: providerConfigure,
	}
}

func providerConfigure(d *schema.ResourceData) (interface{}, error) {
	baseDir := d.Get("base_dir").(string)
	client := fakeservice.NewClient(baseDir)
	return client, nil
}
