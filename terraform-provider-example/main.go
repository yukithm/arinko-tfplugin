package main

import (
	"github.com/hashicorp/terraform/plugin"
	"gitlab.com/yukithm/arinko-tfplugin/terraform-provider-example/example"
)

func main() {
	plugin.Serve(&plugin.ServeOpts{
		ProviderFunc: example.Provider,
	})
}
