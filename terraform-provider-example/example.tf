provider example {
	base_dir = "example-provider-data"
}

resource "example_server" "my_server" {
	address = "172.16.1.11"
}

output "my_server_id" {
	value = "${example_server.my_server.id}"
}
