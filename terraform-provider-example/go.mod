module gitlab.com/yukithm/arinko-tfplugin/terraform-provider-example

go 1.12

require (
	github.com/google/uuid v1.1.1
	github.com/hashicorp/terraform v0.12.6
)
