package fakeservice_test

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"

	"gitlab.com/yukithm/arinko-tfplugin/terraform-provider-example/fakeservice"
)

func TestCreateServer(t *testing.T) {
	baseDir, err := ioutil.TempDir("testdata", "tmp")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(baseDir)

	client := fakeservice.NewClient(baseDir)

	address := "172.16.1.10"
	server, err := client.CreateServer(address)
	if err != nil {
		t.Error(err)
	}

	if server.Address != address {
		t.Errorf("server address: want %v, but %v", address, server.Address)
	}

	jsonFile := filepath.Join(baseDir, "servers", server.ID+".json")
	if !fileExists(jsonFile) {
		t.Errorf("JSON file is not created: %s", jsonFile)
	}
}

func TestGetServer(t *testing.T) {
	baseDir := filepath.Join("testdata")
	client := fakeservice.NewClient(baseDir)

	server, err := client.GetServer("foo123")
	if err != nil {
		t.Error(err)
	}

	want := fakeservice.Server{
		ID:      "foo123",
		Address: "172.16.1.20",
	}

	if *server != want {
		t.Errorf("server: want %v, but %v", want, server)
	}
}

func TestUpdateServer(t *testing.T) {
	baseDir, err := ioutil.TempDir("testdata", "tmp")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(baseDir)

	client := fakeservice.NewClient(baseDir)

	address := "172.16.1.10"
	server, err := client.CreateServer(address)
	if err != nil {
		t.Error(err)
	}

	newAddress := "172.16.1.20"
	server, err = client.UpdateServer(server.ID, newAddress)
	if server.Address != newAddress {
		t.Errorf("server address: want %v, but %v", newAddress, server.Address)
	}

	server, err = client.GetServer(server.ID)
	if server.Address != newAddress {
		t.Errorf("server address: want %v, but %v", newAddress, server.Address)
	}
}

func TestDeleteServer(t *testing.T) {
	baseDir, err := ioutil.TempDir("testdata", "tmp")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(baseDir)

	orig := filepath.Join("testdata", "servers", "foo123.json")
	serversDir := filepath.Join(baseDir, "servers")
	if err := os.MkdirAll(serversDir, 0755); err != nil {
		t.Fatal(err)
	}
	jsonFile := filepath.Join(serversDir, "foo123.json")

	if _, err := copyFile(orig, jsonFile); err != nil {
		t.Fatal(err)
	}

	client := fakeservice.NewClient(baseDir)

	if err := client.DeleteServer("foo123"); err != nil {
		t.Error(err)
	}

	if fileExists(jsonFile) {
		t.Errorf("JSON file is not deleted: %s", jsonFile)
	}
}

func fileExists(file string) bool {
	_, err := os.Stat(file)
	return err == nil
}

func copyFile(src, dst string) (int64, error) {
	srcStat, err := os.Stat(src)
	if err != nil {
		return 0, err
	}

	if !srcStat.Mode().IsRegular() {
		return 0, fmt.Errorf("%s is not a regular file", src)
	}

	srcFile, err := os.Open(src)
	if err != nil {
		return 0, err
	}
	defer srcFile.Close()

	dstFile, err := os.Create(dst)
	if err != nil {
		return 0, err
	}
	defer dstFile.Close()

	return io.Copy(dstFile, srcFile)
}
