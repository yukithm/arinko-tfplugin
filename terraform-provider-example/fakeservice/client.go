package fakeservice

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/google/uuid"
)

var (
	ErrServerNotExist = errors.New("server not exist")
)

type Server struct {
	ID      string
	Address string
}

type Client struct {
	BaseDir string
}

func NewClient(baseDir string) *Client {
	return &Client{
		BaseDir: baseDir,
	}
}

func (c *Client) CreateServer(address string) (*Server, error) {
	server := &Server{
		ID:      uuid.New().String(),
		Address: address,
	}

	if err := c.saveServer(server); err != nil {
		return nil, err
	}

	return server, nil
}

func (c *Client) UpdateServer(id, address string) (*Server, error) {
	if id == "" {
		return nil, errors.New("server ID must not be empty")
	}
	file := c.serverJSONPath(id)
	if !fileExists(file) {
		return nil, ErrServerNotExist
	}

	server := &Server{
		ID:      id,
		Address: address,
	}

	if err := c.saveServer(server); err != nil {
		return nil, err
	}

	return server, nil
}

func (c *Client) GetServer(id string) (*Server, error) {
	var server Server

	file := c.serverJSONPath(id)
	if err := readJSON(file, &server); err != nil {
		if os.IsNotExist(err) {
			return nil, ErrServerNotExist
		}
		return nil, err
	}

	return &server, nil
}

func (c *Client) DeleteServer(id string) error {
	file := c.serverJSONPath(id)
	err := os.Remove(file)
	if err != nil {
		if os.IsNotExist(err) {
			return ErrServerNotExist
		}
		return err
	}

	return nil
}

func (c *Client) saveServer(server *Server) error {
	if server.ID == "" {
		return errors.New("server ID must not be empty")
	}

	file := c.serverJSONPath(server.ID)

	return writeJSON(file, server)
}

func (c *Client) serverJSONPath(id string) string {
	return filepath.Join(c.BaseDir, "servers", id+".json")
}

func readJSON(file string, dest interface{}) error {
	data, err := ioutil.ReadFile(file)
	if err != nil {
		return err
	}

	return json.Unmarshal(data, dest)
}

func writeJSON(file string, obj interface{}) error {
	dir := filepath.Dir(file)
	if err := os.MkdirAll(dir, 0755); err != nil {
		return err
	}

	data, err := json.Marshal(obj)
	if err != nil {
		return err
	}

	return ioutil.WriteFile(file, data, 0644)
}

func fileExists(file string) bool {
	_, err := os.Stat(file)
	return err == nil
}
